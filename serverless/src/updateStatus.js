"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const updateStatus = async (event) => {

let {token, PotId, PlantStatus} = JSON.parse(event.body)

const dynamodb = new AWS.DynamoDB.DocumentClient

var storedId = admin.auth().verifyIdToken(token)

var UserId = (await storedId).uid

//Check device is registed to the user

let checkUserRegisterd = {
    TableName: "PlantPotDB",
    ProjectionExpression: "#PotId, UserId",
    KeyConditionExpression: "#PotId = :PotId",
    FilterExpression: "attribute_exists(UserId) and UserId = :UserId",
    ExpressionAttributeNames: {
      "#PotId" : "PotId",
    },
    ExpressionAttributeValues : {
      ":PotId" : PotId,
      ":UserId" : UserId
    }
  };

  let result = await dynamodb.query(checkUserRegisterd, function(err, data){
    if(err) console.log(err, err.stack)
    else console.log("success")
  }).promise();


  // update the status of the plant
  if(result.Count == 1){
    let appendData = {
        TableName: "PlantPotDB",
        Key: {
          "PotId" : PotId
        },
        UpdateExpression: "SET PlantStatus = :PlantStatus",
        ExpressionAttributeValues: {
          ":PlantStatus" : PlantStatus
        },
        ReturnValues: "ALL_NEW"
      }
    
      await dynamodb.update(appendData, function(err, data){
        if(err) console.log(err, err.stack)
        else console.log("success")
      }).promise();
    
        return {
          statusCode: 200,
          headers: {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin" : "https://joshprojects.site",
            "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
          },
          body: JSON.stringify("Updated Successfully")
        };

  }else{
    return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers" : "Content-Type",
          "Access-Control-Allow-Origin" : "https://joshprojects.site",
          "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
        },
        body: JSON.stringify("User not registered to that ID")
      };
    }
}

module.exports = {
  handler: updateStatus
}


