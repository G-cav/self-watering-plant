"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const addDevice = async (event) => {

let {token, PotId, PlantName, PlantType} = JSON.parse(event.body)
const dynamodb = new AWS.DynamoDB.DocumentClient
var storedId = admin.auth().verifyIdToken(token)


// Table Data
const now = new Date();
let RegisteredOn = now.toLocaleDateString()

var UserId = (await storedId).uid


//BEGIN QUERIES

  // checking that device id exists
  let checkId = {
    TableName: "PlantPotDB",
    ProjectionExpression: "#PotId",
    KeyConditionExpression: "#PotId = :PotId",
    ExpressionAttributeNames: {
      "#PotId" : "PotId"
    },
    ExpressionAttributeValues : {
      ":PotId" : PotId
    }
  };

  // check that device id is not taken
  let checkIdNotTaken = {
    TableName: "PlantPotDB",
    ProjectionExpression: "#PotId, UserId",
    KeyConditionExpression: "#PotId = :PotId",
    FilterExpression: "attribute_not_exists(UserId) and attribute_not_exists(PlantName) and attribute_not_exists(PlantType)",
    ExpressionAttributeNames: {
      "#PotId" : "PotId",

    },
    ExpressionAttributeValues : {
      ":PotId" : PotId
    }
  };

  let appendData = {
    TableName: "PlantPotDB",
    Key: {
      "PotId" : PotId
    },
    UpdateExpression: "SET UserId = :UserId, RegisteredOn = :RegisteredOn, PlantName = :PlantName, PlantType = :PlantType, PlantStatus = :PlantStatus",
    ExpressionAttributeValues: {
      ":UserId" : UserId,
      ":RegisteredOn" : RegisteredOn,
      ":PlantName" : PlantName,
      ":PlantType" : PlantType,
      ":PlantStatus" : "Healthy"
    },
    ReturnValues: "ALL_NEW"
  }

  let checkUserRegisterd = {
    TableName: "PlantPotDB",
    ProjectionExpression: "#PotId, UserId",
    KeyConditionExpression: "#PotId = :PotId",
    FilterExpression: "attribute_exists(UserId) and UserId = :UserId",
    ExpressionAttributeNames: {
      "#PotId" : "PotId",
    },
    ExpressionAttributeValues : {
      ":PotId" : PotId,
      ":UserId" : UserId
    }
  };

  // END QUERIES

  let idQueryResult = await dynamodb.query(checkId, function(err,data) {
    if(err) 
      console.log(err, err.stack);
  }).promise();

    // ID exists
  if(idQueryResult.Count == 1){
    // Check not taken
    let idNotTakenQueryResult = await dynamodb.query(checkIdNotTaken, function(err,data) {
      if(err) 
        console.log(err, err.stack);
    }).promise();

    // already registered
    let UserAlreadyReqisteredResult = await dynamodb.query(checkUserRegisterd, function(err,data) {
        if(err) 
          console.log(err, err.stack);
      }).promise();

    if (idNotTakenQueryResult.Count == 1 ){
      await dynamodb.update(appendData, function(err, data){
        if(err) console.log(err, err.stack)
        else console.log("success")
      }).promise();

      return {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Headers" : "Content-Type",
          "Access-Control-Allow-Origin" : "https://joshprojects.site",
          "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
        },
        body: JSON.stringify("success")
      };

    }else if(UserAlreadyReqisteredResult.Count == 1){
        await dynamodb.update(appendData, function(err, data){
            if(err) console.log(err, err.stack)
            else console.log("success")
          }).promise();

          return {
            statusCode: 200,
            headers: {
              "Access-Control-Allow-Headers" : "Content-Type",
              "Access-Control-Allow-Origin" : "https://joshprojects.site",
              "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
            },
            body: JSON.stringify("success")
          };
          
    }

  }else {
    return {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin" : "https://joshprojects.site",
        "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
      },
      body: JSON.stringify("failed")
    };
  }
};

module.exports = {
  handler: addDevice
}


