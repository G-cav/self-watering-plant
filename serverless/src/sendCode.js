"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")
const SNS = new AWS.SNS();
AWS.config.update({region: "ap-southeast-2"})

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const sendCode = async (event) => {

let {token, PhoneNumber} = JSON.parse(event.body)

const dynamodb = new AWS.DynamoDB.DocumentClient;

var storedId = admin.auth().verifyIdToken(token);

var UserId = (await storedId).uid;

// generate this
var generateRandomNDigits = (n) => {
    return Math.floor(Math.random() * (9 * (Math.pow(10, n)))) + (Math.pow(10, n));
  }
  
let otp = generateRandomNDigits(5)

// store code and phone number in database for verification 
let verify = {
  TableName: "UserDB",
  Key: {
    "UserId" : UserId
  },
  UpdateExpression: "SET OTP = :otp, Verified = :verified,  PhoneNumber = :PhoneNumber",
  ExpressionAttributeValues: {
    ":otp" : otp,
    ":verified" : false,
    ":PhoneNumber" : PhoneNumber
  },
  ReturnValues: "ALL_NEW"
};

  await dynamodb.update(verify, function(err){
      if (err){
          console.log("err", err)
      }
  }).promise();
  

// send the otp to the user  
  let params = {
      PhoneNumber: PhoneNumber,
      Message: `Your Verification code is ${otp}`
  };

  await SNS.publish(params, function(err){
        if(err){
            console.log("err", err)
        }else{
            console.log("SMS has been sent")
        }
  }).promise()

  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Headers" : "Content-Type",
      "Access-Control-Allow-Origin" : "https://joshprojects.site",
      "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
    },
    body: JSON.stringify(`OTP sent ${PhoneNumber}`)
  };
};

module.exports = {
  handler: sendCode
}
