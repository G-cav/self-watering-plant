const AWS = require("aws-sdk")

exports.handler = async (event) => {
const dynamodb = new AWS.DynamoDB.DocumentClient;
const date = new Date();


const dataObject = {
    "Temperature" : event.Temperature,
    "WaterLevel" : event.WaterLevel,
    "Moisture" : event.Moisture,
    "Date" : date.toLocaleDateString()
};

// get items from db

let checkId = {
  TableName: process.env.TABLE_NAME,
  ProjectionExpression: "#PotId, SensorInfo",
  KeyConditionExpression: "#PotId = :PotId",
  ExpressionAttributeNames: {
    "#PotId" : "PotId"
  },
  ExpressionAttributeValues : {
    ":PotId" : event.PotId
  }
};

let result = await dynamodb.query(checkId, function(err,data) {
  if(err) 
    console.log(err, err.stack);
}).promise();

const response = {
        statusCode: 200,
        body: JSON.stringify(result.Count)
};


//  create a new entry if it doesnt exist

if (result.Count == 0){
  await dynamodb.put({
    Item: {
        "PotId" : event.PotId,
        "SensorInfo" : [dataObject]
    },
    TableName: process.env.TABLE_NAME
  }).promise();
  return response
}

// update the data array if it exists
if (result.Count == 1){
  let appendData = {
    TableName: process.env.TABLE_NAME,
    Key: {
      "PotId" : event.PotId
    },
    UpdateExpression: "SET SensorInfo = list_append(SensorInfo, :dataObject)",
    ExpressionAttributeValues: {
      ":dataObject" : [dataObject]
    },
    ReturnValues: "ALL_NEW"
  };


  await dynamodb.update(appendData, function(err, data){
      if(err) console.log(err, err.stack)
      else console.log("success")
  }).promise();
}
    return response;
};
