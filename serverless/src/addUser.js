"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const addUser = async (event) => {

let {token} = JSON.parse(event.body)
const dynamodb = new AWS.DynamoDB.DocumentClient;
var storedId = admin.auth().verifyIdToken(token);

var UserId = (await storedId).uid;

let user = admin.auth().getUser(UserId)
let email = (await user).email

    let NewUser = {
        "UserId" : UserId,
        "Email" : email,
    };

    let register = {
        TableName: "UserDB",
        Item: NewUser,
        ConditionExpression: 'attribute_not_exists(#UserId)',
        ExpressionAttributeNames: { "#UserId" : "UserId"}
    };

    await dynamodb.put(register, function(err, data){
        if (err){
            console.log("err", err)
        }
    }).promise();

    return {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin" : "https://joshprojects.site",
        "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
      },
      body: JSON.stringify("Success")
    };
};

module.exports = {
  handler: addUser
}


