"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const fetchSingleDevice = async (event) => {

let {token, PotId} = JSON.parse(event.body)

const dynamodb = new AWS.DynamoDB.DocumentClient

var storedId = admin.auth().verifyIdToken(token)


var UserId = (await storedId).uid


//BEGIN QUERIES

  // Scan for registered devices
  let fetchDeviceData = {
    TableName: "PlantPotDB",
    ProjectionExpression: "PotId, SensorInfo, PlantName, PlantType, PlantStatus",
    FilterExpression: "UserId = :UserId and PotId = :PotId",
    ExpressionAttributeValues : {
      ":UserId" : UserId,
      ":PotId" : PotId
    }
};

  // END QUERIES

  let result = await dynamodb.scan(fetchDeviceData, function(err, data){
        if(err) console.log(err, err.stack)
        else console.log("success")
      }).promise();

    return {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin" : "https://joshprojects.site",
        "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
      },
      body: JSON.stringify(result)
    };
  }

module.exports = {
  handler: fetchSingleDevice
}


