"use strict";

const AWS = require("aws-sdk")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceAccountKey.json")
const SNS = new AWS.SNS();
AWS.config.update({region: "ap-southeast-2"})

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const verifyUser = async (event) => {

let {token, OTP} = JSON.parse(event.body)

const dynamodb = new AWS.DynamoDB.DocumentClient;

var storedId = admin.auth().verifyIdToken(token);

var UserId = (await storedId).uid;


// get OTP in database
let codeQuery = {
    TableName: "UserDB",
    ProjectionExpression: "OTP, PhoneNumber",
    KeyConditionExpression: "#UserId = :UserId",
    ExpressionAttributeNames: {
      "#UserId" : "UserId",
    },
    ExpressionAttributeValues : {
      ":UserId" : UserId
    }
  };

  let result = await dynamodb.query(codeQuery, function(err){
      if (err){
          console.log("err", err)
      }
  }).promise();
  
  let storedOTP = result.Items[0].OTP
  let PhoneNumber = result.Items[0].PhoneNumber
  // send message

  if(storedOTP == OTP){
    // set verified to true
    let verify = {
        TableName: "UserDB",
        Key: {
          "UserId" : UserId
        },
        UpdateExpression: "SET Verified = :verified",
        ExpressionAttributeValues: {
          ":verified" : true,
        },
        ReturnValues: "ALL_NEW"
      };

      await dynamodb.update(verify, function(err){
        if (err){
            console.log("err", err)
        }
    }).promise();

    // send message that it suceeded 
    let params = {
        PhoneNumber: PhoneNumber,
        Message: `Thank you for registering your phone number`
    };

    await SNS.publish(params, function(err){
            if(err){
                console.log("err", err)
            }else{
                console.log("SMS has been sent")
            }
    }).promise()

}

  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Headers" : "Content-Type",
      "Access-Control-Allow-Origin" : "https://joshprojects.site",
      "Access-Control-Allow-Methods" : "OPTIONS,POST,GET"
    },
    body: JSON.stringify("Sent successfully, if valid a text was sent")
  };
};

module.exports = {
  handler: verifyUser
}
