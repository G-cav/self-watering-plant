var addPlant;
(function (addPlant) {
    "use strict";
    let w = window;
    let loggedIn = false;
    function editUser(user) {
        console.log(user);
        let userTitle = new JTML('#userTitle')
            .html(user.displayName);
        let phoneNumberForm = new JTML('#phoneNumberForm')
            .on('submit', async (e) => {
            e.preventDefault();
            let authToken = await w.auth.currentUser.getIdToken(true);
            let SMSdata = {
                "token": authToken,
                "PhoneNumber": phoneNumberInput.value.replace(/\s/g, '')
            };
            console.log("phone number data being sent:");
            console.log(SMSdata);
            let SMSResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/sms', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(SMSdata)
            }).then((res) => res.json());
            console.log("this is the result after sending a phone number:");
            console.log(SMSResult);
            let authCodeForm = new JTML('#authCodeForm')
                .on('submit', async (e) => {
                e.preventDefault();
                let authToken = await w.auth.currentUser.getIdToken(true);
                let verifyData = {
                    "token": authToken,
                    "OTP": authCodeInput.value
                };
                console.log("verification data being sent:");
                console.log(verifyData);
                let verifyResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/sms/verify', {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify(verifyData)
                }).then((res) => res.json());
                console.log("this is the result after sending a the verification:");
                console.log(verifyResult);
            });
            let authCodeInput = new JTML('input')
                .set('type', 'text')
                .set('placeholder', 'insert one time passcode here')
                .css({ 'margin-top': '100px' })
                .appendTo(authCodeForm);
            let authCodeSubmit = new JTML('input')
                .set('type', 'submit')
                .appendTo(authCodeForm);
        });
        let phoneNumberInput = new JTML('input')
            .set('type', 'text')
            .set('placeholder', '+61 0404 123 456')
            .appendTo(phoneNumberForm);
        let phoneNumberSubmit = new JTML('input')
            .set('type', 'submit')
            .appendTo(phoneNumberForm);
    }
    w.onAuthStateChanged(w.auth, (user) => {
        if (user) {
            if (!loggedIn) {
                loggedIn = true;
                editUser(user);
            }
        }
        else {
            w.signInWithPopup(w.auth, w.provider)
                .then(async (result) => {
                const user = result.user;
                console.log(user);
                let authToken = await w.auth.currentUser.getIdToken(true);
                let loginData = {
                    "token": authToken
                };
                let loginResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/register', {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify(loginData)
                }).then((res) => res.json());
                console.log("this is the response after first logging in and telling the server:");
                console.log(loginResult);
            }).catch((error) => {
                alert(error);
            });
        }
    });
})(addPlant || (addPlant = {}));
