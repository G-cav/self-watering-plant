namespace addPlant {
"use strict";

let w = window as any;
let loggedIn = false;

async function viewPlant(user) {
	let currentPotID = new URL(window.location.href).searchParams.get('p');
	let currentPlant;
	let authToken = await w.auth.currentUser.getIdToken(true)
	let dataToSend = {
		"token": authToken,
		"PotId": currentPotID
	}
	let result = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/fetch/single', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify(dataToSend)
	}).then((res) => res.json());
	currentPlant = result.Items[0];
	console.log(`this is all of the data associated with the pot '${currentPotID}', which is associated with this user:`)
	console.log(result);

	let potName = new JTML('#potName')
		.html(currentPlant.PlantName)

	console.log("heloo there");
	let statusForm = new JTML('#statusForm')
		.on('submit',async (e)=>{
			e.preventDefault();
			let healthyChecked = new JTML('#healthyCheck').ref.checked;
			let unhealthyChecked = new JTML('#unhealthyCheck').ref.checked;
			console.log(healthyChecked);
			console.log(unhealthyChecked);
			if (healthyChecked ^ unhealthyChecked) {
				let authToken = await w.auth.currentUser.getIdToken(true)
				let dataToSend = {
					"token": authToken,
					"PlantStatus": null,
					"PotId": currentPlant.PotId
				}
				if (healthyChecked) {
					dataToSend.PlantStatus = 'Healthy';
				} else {
					dataToSend.PlantStatus = 'Unhealthy';
				}
				console.log('the data being sent to the server is')
				console.log(dataToSend);
				let plants = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/update', {
					method: 'POST',
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify(dataToSend)
				}).then((res) => res.json());
				console.log("this is all of the plants associated with this user:")
				console.log(plants);
			} else {
				alert('you need to select either healthy or unhealthy in order to submit')
			}
		})

	let imageButton = new JTML('#imageButton')
		.on('click',(e)=>{
			alert("not configured")
			console.log("load up the image taking thing so you can send an image with your submission");
		})
}

w.onAuthStateChanged(w.auth, (user) => {
	if (user) {
		if (!loggedIn) {
			loggedIn = true;
			viewPlant(user);
		}
	} else {
		w.signInWithPopup(w.auth, w.provider)
		.then(async (result) => {
			const user = result.user;
			console.log(user);
			let authToken = await w.auth.currentUser.getIdToken(true)
			let loginData = {
				"token": authToken
			}
			let loginResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/register', {
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(loginData)
			}).then((res) => res.json());
			console.log("this is the response after first logging in and telling the server:")
			console.log(loginResult);
		}).catch((error) => {
			alert(error);
		});	
	}
});

}

