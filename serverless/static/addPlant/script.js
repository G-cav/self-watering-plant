var addPlant;
(function (addPlant_1) {
    "use strict";
    let w = window;
    let loggedIn = false;
    function addPlant(user) {
        let addRow = new JTML('#addRowForm')
            .on('submit', async (e) => {
            e.preventDefault();
            let authToken = await w.auth.currentUser.getIdToken(true);
            let dataToSend = {
                "token": authToken,
                "PotId": potCodeInput.value,
                "PlantName": plantNameInput.value,
                "PlantType": plantTypeInput.value
            };
            console.log("data being sent:");
            console.log(dataToSend);
            let result1 = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(dataToSend)
            }).then((res) => res.json());
            console.log("response:");
            console.log(result1);
            if (result1 == "success") {
                alert(`success, pot number '${potCodeInput.value}' is now registered as a '${plantTypeInput.value}' called ${plantNameInput.value} `);
                potCodeInput.setValue('');
                plantNameInput.setValue('');
                plantTypeInput.setValue('');
            }
            else {
                alert(result1);
            }
        });
        let potCodeContainer = new JTML('div')
            .css({ 'padding': '20px' })
            .class('row')
            .appendTo(addRow);
        let potCodeTitle = new JTML('h2')
            .html('Pot Code')
            .appendTo(potCodeContainer);
        let potCodeRow = new JTML('div')
            .css({
            'display': 'grid',
            'grid-template-columns': '85px 60px 1fr',
            'align-items': 'center'
        })
            .appendTo(potCodeContainer);
        let imgContainer = new JTML('div')
            .css({ 'cursor': 'pointer' })
            .appendTo(potCodeRow)
            .on('click', () => {
            alert("not configured");
            console.log("you should scan the QR code without loading a new page here and then store the value somewhere nice");
        });
        let cameraImg = new JTML('img')
            .css({
            'width': '100%',
            'position': 'relative',
            'top': '10px'
        })
            .set('src', '../photo_camera_black_24dp.svg')
            .appendTo(imgContainer);
        let QRCodeText = new JTML('p')
            .html('scan QR code')
            .css({
            'font-size': '0.8em',
            'text-align': 'center',
            'line-height': '1.2'
        })
            .appendTo(imgContainer);
        let or = new JTML('p')
            .html('or')
            .css({ 'place-self': 'center' })
            .appendTo(potCodeRow);
        let potCodeInput = new JTML('input')
            .set('type', 'text')
            .set('placeholder', 'write code here')
            .css({
            'min-width': '0px',
            'margin-right': '10px',
            'padding-left': '10px'
        })
            .appendTo(potCodeRow);
        // ----------------------------
        let plantTypeContainer = new JTML('div')
            .css({ 'padding': '20px' })
            .class('row')
            .appendTo(addRow);
        let plantTypeTitle = new JTML('h2')
            .html('Plant Type')
            .appendTo(plantTypeContainer);
        let plantTypeRow = new JTML('div')
            .css({
            'display': 'grid',
            'grid-template-columns': '85px 60px 1fr',
            'align-items': 'center'
        })
            .appendTo(plantTypeContainer);
        let plantTypeimgContainer = new JTML('div')
            .css({ 'cursor': 'pointer' })
            .appendTo(plantTypeRow)
            .on('click', () => {
            alert("not configured");
            console.log("take a picture of a plant and process it here");
        });
        let plantTypecameraImg = new JTML('img')
            .css({
            'width': '100%',
            'position': 'relative',
            'top': '10px'
        })
            .set('src', '../photo_camera_black_24dp.svg')
            .appendTo(plantTypeimgContainer);
        let plantTypeQRCodeText = new JTML('p')
            .html('photograph plant')
            .css({
            'font-size': '0.8em',
            'text-align': 'center',
            'line-height': '1.2'
        })
            .appendTo(plantTypeimgContainer);
        let plantTypeor = new JTML('p')
            .html('or')
            .css({ 'place-self': 'center' })
            .appendTo(plantTypeRow);
        let plantTypeInput = new JTML('input')
            .set('type', 'text')
            .set('placeholder', 'write type here')
            .css({
            'min-width': '0px',
            'margin-right': '10px',
            'padding-left': '10px'
        })
            .appendTo(plantTypeRow);
        // ------------------------------------
        let everythingElseContainer = new JTML('div')
            .css({ 'padding': '20px' })
            .appendTo(addRow);
        let plantName = new JTML('h2')
            .html('Plant Name')
            .appendTo(everythingElseContainer);
        let plantNameInput = new JTML('input')
            .set('type', 'text')
            .set('placeholder', 'give it a nickname')
            .css({
            'min-width': '0px',
            'margin-right': '60px',
            'padding-left': '10px',
            'box-sizing': 'border-box',
            'width': 'calc(100% - 10px)',
            'height': '35px'
        })
            .appendTo(everythingElseContainer);
        let submitButton = new JTML('input')
            .set('type', 'submit')
            .class('hoverableButton')
            .css({
            'border-radius': '7px',
            'max-width': 'fit-content',
            'place-self': 'center',
            'margin': '20px'
        })
            .appendTo(addRow);
    }
    w.onAuthStateChanged(w.auth, (user) => {
        if (user) {
            if (!loggedIn) {
                loggedIn = true;
                addPlant(user);
            }
        }
        else {
            w.signInWithPopup(w.auth, w.provider)
                .then(async (result) => {
                const user = result.user;
                console.log(user);
                let authToken = await w.auth.currentUser.getIdToken(true);
                let loginData = {
                    "token": authToken
                };
                let loginResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/register', {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify(loginData)
                }).then((res) => res.json());
                console.log("this is the response after first logging in and telling the server:");
                console.log(loginResult);
            }).catch((error) => {
                alert(error);
            });
        }
    });
})(addPlant || (addPlant = {}));
