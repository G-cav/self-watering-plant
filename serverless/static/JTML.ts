// Josh Text Markup Language
// simplified DOM manipulation
// since (almost) all methods returns 'this' you can chain multiple methods one after the other
// last edited 2/10/2021
class JTML {

	#ref:any;

	constructor(type: string|Element) {
		this.ref = type;
	}

	get ref() {
		return this.#ref;
	}

	set ref(type) {
		if (/[.#:]\w+/.test(type)) {
			this.#ref = document.querySelector(type);
		} else if (type instanceof Element) {
			this.#ref = type;
		} else {
			this.#ref = document.createElement(type);
		}
	}

	public getHtml() {
		return this.ref.innerHTML;
	}

	get value() {
		if (this.#ref instanceof HTMLInputElement) {
			return this.#ref.value;
		}
	}

	// needs to be a function so that we can return 'this'
	public setValue(newValue: string) {
		if (this.#ref instanceof HTMLInputElement) {
			this.#ref.value = newValue;
		}
		return this;
	}

	// set the innherHTML of the element, reminder that 
	// this will remove all event listeners on this element and its children
	public html(value: String) {
		this.ref.innerHTML = value;
		return this;
	}

	// assign any arbitrary property
	set(key: string, value: any) {
		this.#ref.setAttribute(key, value);
		return this;
	}

	// assign CSS style to this particular element, provided in JSON notation
	// e.g. {
	//    "background-color": "red",
	//    "opacity": 0.5
	// }
	public css(cssObj: Object) {
		for (const [key, value] of Object.entries(cssObj)) {
			this.ref.style[key] = value;
		}
		return this;
	}

	// // can be passed either a single key:value pair or an array of arrays in the format [[key,value],[key,value]]
	// css(key: string,value: string=null) {
	// 	if (Array.isArray(key)) {
	// 		key.forEach((pair)=>{
	// 			this.#ref.style[pair[0]] = pair[1];
	// 		})
	// 	} else {
	// 		this.#ref.style[key] = value;
	// 	}
	// 	return this;
	// }

	// cssToggle(state1:string,state2:string) {
	// 	if (this.#ref.style[state1[0]] == state1[1]) {
	// 		this.#ref.style[state2[0]] = state2[1];
	// 	} else {
	// 		this.#ref.style[state1[0]] = state1[1];
	// 	}
	// 	return this;
	// }

	// // same as css, but applies to hover state
	// hover(key,value=null) {

	// }

	// add a class to the element
	// this could be achieved using set() but its just cleaner this way and it's a property you often want to assign
	public class(className: string|Array<string>) {
		if (Array.isArray(className)) {
			className.forEach((singleClass)=>{
				this.#ref.classList.add(singleClass);
			})
		} else {
			this.#ref.classList.add(className);
		}
		return this;
	}

	// remove class from the element
	public removeClass(className: string) {
		this.#ref.classList.remove(className);
		return this;
	}

	public toggleClass(className: string) {
		this.#ref.classList.toggle(className);
		return this;
	}

	// append a child element to the end of this element
	public add(JTMLarray: JTML|Array<JTML>) {
		if (Array.isArray(JTMLarray)) {
			JTMLarray.forEach((singleJTML)=>{
				this.#ref.append(singleJTML.ref);
			})
		} else {
			this.#ref.append(JTMLarray.ref);
		}
		return this;
	}

	// append this element as the last child to another
	public appendTo(parentElem: JTML) {
		parentElem.add(this);
		return this;
	}

	// passes all of the input parameters to addEventListener
	// just saves you from writing varName.ref.addEventListener
	public addEventListener(type: any,listener: (this: HTMLElement, ev: any) => any,options?: boolean | AddEventListenerOptions) {
		this.#ref.addEventListener(type,listener,options);
		return this;
	}

	// a duplicate of addEventListener, you can probably have them replicating each other
	// more cleanly but cant be bothered right now
	public on(type: any,listener: (this: HTMLElement, ev: any) => any,options?: boolean | AddEventListenerOptions) {
		this.#ref.addEventListener(type,listener,options);
		return this;
	} 

	// removes all event listeners on this element and its children
	public removeEventListeners() {
		var new_element = this.#ref.cloneNode(true);
		this.#ref.parentNode.replaceChild(new_element, this.ref);
		this.#ref = new_element;
		return this;
	}

	// remove this element from the DOM
	public remove() {
		this.ref.remove();
		return false;
	}

	// maybe try implementing this in a different way, but this version
	// would end up selecting unintuitive elements because querySelector operates depth-first
	// it should probably use the Element.children property because that's a shallow selection
	// and then perform it iteratively
	// // perform a querySelector for a given string
	// public child(childIdentifier: string) {
	// 	return new JTML(this.ref.querySelector(childIdentifier));
	// }

	// perform a querySelectorAll for a given string
	// by default select all children
	public children(childIdentifier = '*') {
		let children = [];
		let preChildren = this.ref.querySelectorAll(childIdentifier);
		preChildren.forEach((singleChild)=>{
			children.push(new JTML(singleChild));
		})
		return children;
	}

	public parent() {
		return new JTML(this.ref.parentNode);
	}
}