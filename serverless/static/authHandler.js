// when adding the various login modes, the order you add them is the order they appear within the div
// last edited 30/01/2021
class AuthHandler {
	constructor(firebaseConfig) {
		if (typeof firebase === "undefined" || typeof firebase.auth === "undefined") {
			console.log("should do something here to make a visual user interface indicating something is wrong")
			console.log("you need to load the two scripts first in HTML, perhaps with a more recent version number");
			console.log('<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>');
			console.log('<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>')
		} else {
			this.firebaseConfig = firebaseConfig;
			this.provider;
			this.htmlContainerRef;
			this.buttonHolder;
			this.errorFields = [];
			this.modes = [];
			this.user;
			this.accountDetails;
			this.loggingIn = ()=>{ return true; };
			this.loggingOut = ()=>{ return true; };
			
			firebase.initializeApp(this.firebaseConfig);
			this.auth = firebase.auth();
			
			// note that these fire once the page loads
			this.auth.onAuthStateChanged((user) => {		 
				this.user = user;
				this.resetLogin();
				if (user) {
					this.loggingIn(this);
				} else {
					this.loggingOut(this);
				}
			});
		}
	}

	resetLogin() {
		document.authForm.signUpEmail.value = '';
		document.authForm.signUpPassword.value = '';
		document.authForm.passwordAgain.value = '';
		document.authForm.nickname.value = '';
		document.querySelector(`#showLogin`).style.display = 'block';
		document.querySelector(`#showSignUp`).style.display = 'block';
		document.authForm.style = 'display:none;';
		this.resetErrorFields();
	}

	configAuthForm(htmlContainerRef) {
		this.htmlContainerRef = htmlContainerRef;
		let buttonHolder = document.createElement("div");
		buttonHolder.id = 'buttonHolder';
		buttonHolder.style.display = 'grid';
		let authForm = document.createElement("form");
		authForm.name = 'authForm';
		authForm.id = 'authForm';
		authForm.style.display = 'none';
		htmlContainerRef.append(buttonHolder);
		htmlContainerRef.append(authForm);
		this.buttonHolder = buttonHolder;
	}

	addOr(mode) {
		this.modes.push(mode);
		if (this.modes[0] != mode) {
			let or = document.createElement("span");
			or.className = 'or';
			or.innerHTML = "OR";
			this.buttonHolder.append(or);
		}
	}

	resetErrorFields() {
		this.errorFields.forEach((errorField)=>{
			errorField.style = 'display: none; max-height:0px; margin-bottom: 0px;';
		})
	}

	configEmailLogin() {
		this.addOr('email');
		let showSignUp = document.createElement("button");
		showSignUp.id = 'showSignUp';
		showSignUp.innerHTML = 'new user <strong>sign-up</strong>'
		let showLogin = document.createElement("button");
		showLogin.id = 'showLogin';
		showLogin.innerHTML = 'returning user <strong>login</strong>'
		this.buttonHolder.append(showSignUp);
		this.buttonHolder.append(showLogin);

		let signUpEmailLabel = document.createElement("label");
		signUpEmailLabel.innerHTML = 'email';
		signUpEmailLabel.setAttribute('for','signUpEmail');
		let signUpEmail = document.createElement("input");
		signUpEmail.type = 'email';
		signUpEmail.name = 'signUpEmail';
		signUpEmail.id = 'signUpEmail';
		signUpEmail.required = true;
		let emailError = document.createElement("div");
		emailError.id = 'emailError';
		document.authForm.append(signUpEmailLabel)
		document.authForm.append(signUpEmail);
		document.authForm.append(emailError);

		let signUpPasswordLabel = document.createElement("label");
		signUpPasswordLabel.innerHTML = 'password';
		signUpPasswordLabel.setAttribute('for','signUpPassword');
		let signUpPassword = document.createElement("input");
		signUpPassword.type = 'password';
		signUpPassword.name = 'signUpPassword';
		signUpPassword.id = 'signUpPassword';
		signUpPassword.required = true;
		let passwordError = document.createElement("div");
		passwordError.id = 'passwordError';
		document.authForm.append(signUpPasswordLabel)
		document.authForm.append(signUpPassword);
		document.authForm.append(passwordError);

		let signUpContainer = document.createElement("div");
		signUpContainer.id = 'signUpContainer';
		document.authForm.append(signUpContainer);

		let passwordAgainLabel = document.createElement("label");
		passwordAgainLabel.innerHTML = 'repeat password';
		passwordAgainLabel.setAttribute('for','passwordAgain');
		let passwordAgain = document.createElement("input");
		passwordAgain.type = 'password';
		passwordAgain.name = 'passwordAgain';
		passwordAgain.id = 'passwordAgain';
		let againError = document.createElement("div");
		againError.id = 'againError';
		signUpContainer.append(passwordAgainLabel)
		signUpContainer.append(passwordAgain);
		signUpContainer.append(againError);

		let nicknameLabel = document.createElement("label");
		nicknameLabel.innerHTML = 'display name';
		nicknameLabel.setAttribute('for','nickname');
		let nickname = document.createElement("input");
		nickname.type = 'text';
		nickname.name = 'nickname';
		nickname.id = 'nickname';
		signUpContainer.append(nicknameLabel)
		signUpContainer.append(nickname);

		let authSubmit = document.createElement("input");
		authSubmit.type = 'submit';
		document.authForm.append(authSubmit)

		this.errorFields.push(emailError);
		this.errorFields.push(passwordError);
		this.errorFields.push(againError);

		showLogin.addEventListener('click',()=>{
			document.authForm.style.display = 'grid';
			signUpContainer.style.display = 'none';
			showLogin.style.display = 'none';
			showSignUp.style.display = 'block';
			authSubmit.value = 'login';
			document.authForm.scrollIntoView(true, {behavior:"smooth",block: 'start'});
		})
	
		showSignUp.addEventListener('click',()=>{
			document.authForm.style.display = 'grid';
			signUpContainer.style.display = 'grid';
			showLogin.style.display = 'block';
			showSignUp.style.display = 'none';
			authSubmit.value = 'sign up'
			document.authForm.scrollIntoView(true, {behavior:"smooth",block: 'start'});
		})

		document.authForm.addEventListener('submit',(e)=>{
			e.preventDefault();
			if (authSubmit.value == 'sign up') {
				if (signUpPassword.value==passwordAgain.value) {
					this.accountDetails = nickname.value;
					console.log(nickname.value);
					this.auth.createUserWithEmailAndPassword(document.authForm.signUpEmail.value, document.authForm.signUpPassword.value)
					.then((cred) => {
						// don't need to run this.loggingIn here, because the auth state listener will fire
					})
					.catch((err)=>{
						this.resetErrorFields();
						console.log(err);
						emailError.style = 'display: inline-block;';
						emailError.innerHTML = `<p>${err}</p>`;
						authSubmit.value = "sign up";
					});
					authSubmit.value = "loading...";
				} else {
					this.resetErrorFields();
					if (passwordAgain.value.length==0) {
						console.log("no password added");
						againError.style = 'display: inline-block;';
						againError.innerHTML = `<p>Error: Repeat password field empty</p>`;
					} else {
						console.log("the two passwords don't match");
						againError.style = 'display: inline-block;';
						againError.innerHTML = `<p>Error: The two passwords don't match</p>`;
					}
				}	
			} else {
				this.auth.signInWithEmailAndPassword(document.authForm.signUpEmail.value, document.authForm.signUpPassword.value)
				.then((cred) => {
					// don't need to run this.loggingIn here, because the auth state listener will fire
				})
				.catch((err) => {
					this.resetErrorFields();
					console.log(err);
					if (err=="Error: The email address is badly formatted.") {
						reset
						emailError.style = 'display: inline-block;';
						emailError.innerHTML = `<p>${err}</p>`;
					} else {
						passwordError.style = 'display: inline-block;';
						passwordError.innerHTML = `<p>${err}</p>`;
					}
					authSubmit.value = "login";
				});
				authSubmit.value = "loading...";
			}
		})
	}

	configGoogleLogin() {
		this.addOr('google');
		let googleLoginButton = document.createElement("button");
		googleLoginButton.id = 'googleLoginButton';
		googleLoginButton.name = 'googleLoginButton';
		googleLoginButton.innerHTML = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 48 48" class="abcRioButtonSvg"><g><path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path><path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path><path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path><path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path><path fill="none" d="M0 0h48v48H0z"></path></g></svg>Sign in with <strong>Google</strong>`;
		googleLoginButton.addEventListener('click',this.doGoogleLogin)
		this.buttonHolder.append(googleLoginButton);
	}

	doGoogleLogin() {
		this.provider = new firebase.auth.GoogleAuthProvider();
		firebase.auth().signInWithPopup(this.provider).then((result)=>{
			// don't need to run this.loggingIn here, because the auth state listener will fire
		}).catch(function(error) {
			console.log(error);
			console.log("double check that you've got google auth configured as a valid auth method");
		});
	}

	logout() {
		this.auth.signOut().then(() => {
			// don't need to run this.loggingOut here, because the auth state listener will fire
		});
	}

	onceLoggedOut(callbackFunction) {
		this.loggingOut = callbackFunction;
	}

	onceLoggedIn(callbackFunction) {
		this.loggingIn = callbackFunction;
	}
}