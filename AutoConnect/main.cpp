/*
  Author: Alexander Linden
  Title: Smart watering basic setup
  Notes: Please ensure "Seeed water level switch" is pulled to ground
  Internal ESP pullup may not be sufficient to get a true water level reading
  A MOSFET or relay is required to switch the pump on/off
  Moisture probe reference: https://www.dfrobot.com/product-599.html
  Autoconnect by Hieromon
 */

#include "secrets.h"
#include <Arduino.h>
#include <AutoConnect.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <DHT.h>  
#include <ESPmDNS.h> 

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  60        /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;

const int pumpPin = 32;           //Pump control pin
const int moisturePin = 33;       //Soil moisture probe
const int waterSwitchPin = 34;    //water level switch
const int DHTPin = 14;            //the temp/humidity sensor readout

//Function testing
bool wSwitch;
bool pumpBool;
size_t moistureVal;
size_t ambientTemp = 0;
size_t ambientHumidity = 0;

//Function globals
size_t pumpDuration;

DHT dht(DHTPin, DHT11);           //pin 14 is a DHT11

//deep sleep control
bool durationRecieved = false;

//PubSub
// String macAddress = String(WiFi.macAddress());
String SUBSCIRBE_TOPIC = "SWE30012/" + String(WiFi.macAddress()) + "/pump";

//MQTT globals
#define AWS_IOT_PUBLISH_TOPIC   "SWE30012/water/data"
//#define AWS_IOT_SUBSCRIBE_TOPIC sub_topic
const char* AWS_IOT_SUBSCRIBE_TOPIC = SUBSCIRBE_TOPIC.c_str();

//Const char for recieving pumpDuration THROUGH MQTT
const char* pumpDur;

WiFiClientSecure net = WiFiClientSecure();
PubSubClient client(net);
//MQTT End

//Autoconnect Globals
WebServer Server;         
AutoConnect Portal(Server);
AutoConnectConfig AutoConfig;
//
#define AC_DEBUG //enable Autoconnect Debug in serial monitor

//function prototypes
bool waterSwitchLogic ();
bool pumpControl (size_t pumpDuration);
size_t moistureProbe ();
void connectAWS();
void publishMessage();
void messageHandler(char* topic, byte* payload, unsigned int length);
void deepSleepControl();
void print_wakeup_reason();

//Why does this break when a forward decleration like void rootpage(); is implemented, with rootpage() declared at the bottom of the program?
void rootPage() {
  char content[] = "Please work you bitch";
  Server.send(200, "text/plain", content);
}

void guiPage() {
  const char index_html[] PROGMEM = R"rawliteral(
  <!DOCTYPE HTML><html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      body { text-align:center; }
      .vert { margin-bottom: 10%; }
      .hori{ margin-bottom: 0%; }
      table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      margin-left: auto;
      margin-right: auto;
      }
    </style>
  </head>
  <body>
    <div id="container">
      <h2>Smart Watering GUI</h2>
      <p>Patience with embedded hardware is a virtue</p>
      <p>
        <button onclick="getData()">Update Data: Takes ~5s</button>
        <button onclick="sendToRDS()">Send To IoT Core</button>
      </p>
      <p id="temp"></p>
      <p id="rds"></p>
    </div>
  </body>
  <script>
    function getData() {
      var thr = new XMLHttpRequest();
      thr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("temp").innerHTML = this.responseText;
          console.log(thr.response);
        }
      }
      thr.open('GET', "/getData", true);
      thr.send();
      }
      function sendToRDS() {
      var thr = new XMLHttpRequest();
      thr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("rds").innerHTML = this.responseText;
          console.log(thr.response);
        }
      }
      thr.open('GET', "/sendToRDS", true);
      thr.send();
      } 
  </script>
  </html>)rawliteral";
  Server.send(200, "html", index_html);
}

void setup() {
  
  delay(1000);
  Serial.begin(115200);
  AutoConfig.psk = "";
  AutoConfig.apid = "Smart Watering";
  
  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  Portal.config(AutoConfig);

  // initialise DHT11 Temp sensor
  dht.begin();

  //IO for actuators and sensors
  pinMode (pumpPin, OUTPUT);
  pinMode (waterSwitchPin, INPUT_PULLUP);
  //NOTE soil moisture pin is analog so no pinMode

  Server.on("/", guiPage);
  Server.on("/gui", guiPage);
  Server.on("/sendToRDS", publishMessage);
  if (Portal.begin()) {
    Serial.println("WiFi connected: " + WiFi.localIP().toString());
    if (MDNS.begin("SmartWatering")) {
      Serial.println("MDNS Started at SmartWatering.local");
      MDNS.addService("http", "tcp", 80);
    }
  }
  if(WiFi.status() == WL_CONNECTED) {
    connectAWS();
  }



  //reading water leve, temperature and humidity, soil moisture
  //This happens every time the ESP32 boots
  //Also happens each time it exists deep sleep
  moistureVal = moistureProbe();  //read soil moisture

  float temp = dht.readTemperature();
  delay(1000); //wait for DHT sensor

  float hum = dht.readHumidity();
  delay(1000); //wait for DHT sensor
  ambientTemp = temp;
  Serial.println(temp);
  ambientHumidity = hum;
  Serial.println(hum);

  wSwitch = waterSwitchLogic(); //read water level THIS IS NOT VERY ACCURATE =(
}

void loop() {
  Portal.handleClient();
  //publishMessage(); //uncommont this to run then go into deep sleep every TIME_TO_SLEEP seconds
  client.loop();
  delay(1000);
  if(durationRecieved) {  //if pump duration recieved, run pump, go to deep sleep for X duration
    bool t = pumpControl(pumpDuration);

    durationRecieved = false;

    //deep sleep for 5 mins
    Serial.println("Going to sleep now");
    delay(1000);
    Serial.flush(); 
    esp_deep_sleep_start();
    Serial.println("This will never be printed");
  }

}

bool waterSwitchLogic () {
  bool result;
  int waterSwitchState;
  int count = 0;
  for (int i =0; i<500; i++){
    waterSwitchState = digitalRead(waterSwitchPin);
    if (waterSwitchState == HIGH) {     
      count++;
    } 
    delay(10); //delay 0.01s between readings
  }
  if(count == 0) {
    //if only 0s, water level is LOW
    Serial.println("0");
    result = 0; 
  } else {
    //if anything but 0 is seen in a given interval, water level is adequate
    Serial.println("1");
    result = 1;
  }
  delay(200);
  count = 0; //reset count

  return result;
}

bool pumpControl (size_t pumpDuration) {

  pumpDuration = pumpDuration*1000; //Duration to milliseconds (was in s)
  digitalWrite(pumpPin, HIGH);
  Serial.println("On");
  delay(pumpDuration);
  digitalWrite(pumpPin, LOW);
  Serial.println("Off");
  delay(500);

}

size_t moistureProbe () {
  size_t val = analogRead(moisturePin);
  Serial.println("New moisture data: ");
  Serial.println(val);
  delay (1000);
  return val;
}


void connectAWS()
{
  // Autoconnect handles Wifi connection. Uncomment this if removing Autoconnect
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  // Serial.println("Connecting to Wi-Fi");

  // while (WiFi.status() != WL_CONNECTED){
  //   delay(500);
  //   Serial.print(".");
  // }

  // Configure WiFiClientSecure to use the AWS IoT device credentials
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);

  // Connect to the MQTT broker on the AWS endpoint
  client.setServer(AWS_IOT_ENDPOINT, 8883);

  // Create a message handler
  client.setCallback(messageHandler);

  Serial.print("Connecting to AWS IOT");

  while (!client.connect(THINGNAME)) {
    Serial.print(".");
    delay(100);
  }

  if(!client.connected()){
    Serial.println("AWS IoT Timeout!");
    return;
  }

  // Subscribe to a topic
  client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);

  Serial.println("AWS IoT Connected!");
}

void publishMessage()
{
  StaticJsonDocument<200> doc;
  doc["PotId"] = WiFi.macAddress();
  doc["Temperature"] = ambientTemp;
  doc["Humidity"] = ambientHumidity;
  doc["WaterLevel"] = wSwitch;    //1 for full, 0 for empty
  doc["Moisture"] = 120;
  char jsonBuffer[512];
  serializeJson(doc,jsonBuffer);
  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);

  delay(500);

  //wait for watering duration
}

void messageHandler(char* topic, byte* payload, unsigned int length) {
  Serial.print("incoming: ");
  Serial.println(topic);

  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload); //handle deserialisation, error is a bool
  if(error) {
    Serial.println("Some error deserialising JSON");
  }

  pumpDur = doc["Duration"];
  Serial.println(pumpDur);
  pumpDuration = atoi(pumpDur); //char to unsigned int
  durationRecieved = true; //pump duration recieved
}


void deepSleepControl() {
  Serial.println("Deep sleep");
}


void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}
