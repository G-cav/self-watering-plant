# IoT_Launcher_Project
A repository for the "Smart watering system" For the subject IoT Launcher Project
# Smart Watering Technical Documentation

## Contents

## Section 1 - Microcontroller Configuration
To communicate with the backend, our ESP32 uses a library called ‘PubsubClient’. This is an open source library written by Nick O’leary. This library uses certificates provisioned through AWS IoT core to validate it’s connection persistence. 

![2021-11-06_14h52_03](https://user-images.githubusercontent.com/68890346/140597453-962a26cc-2403-4b28-884e-88ca57f27533.png)

The ‘net’ object is of type WifiClient, a native ESP32 package that maintains a connection to a user’s wifi access point. The auxiliary functions used to connect to AWS are the AWS Root certificate (AWS_CERT_CA), AWS device certificate (AWS_CERT_CRT) and AWS private key (AWS_CERT_PRIVATE).

![2021-11-06_15h00_33](https://user-images.githubusercontent.com/68890346/140597460-6d75cbe4-e2bf-4217-a1bf-48e8eb7972ea.png)

The ‘client’ object is our PubSubClient object.
Above, we are defining the AWS IoT Core endpoint our device connects to. 
We then set our callback method to a messageHandler function, this allows us to receive data from AWS IoT core.

![2021-11-06_15h02_48](https://user-images.githubusercontent.com/68890346/140597463-7d46408d-6697-4467-a764-8b20edfe7d01.png)

Finally we connect to AWS IoT core and subscribe to our defined topic. This allows us to accept data via MQTT.



## Section 2 - Configuring and deploying serverless

These installation instructions are assuming you are using a linux based OS, however they can still
be applied to any OS

1. Configure an AWS IAM Account with access to the following services: \
	&nbsp; a. DynamoDB\
	&nbsp; b. S3\
	&nbsp; c. CloudFront\
	&nbsp; d. IoT Core\
	&nbsp; e. CloudWatch\
	&nbsp; f. SNS\
	&nbsp; g. Lambda
	
2. Install the serverless node modules from the package-lock.json when in the `/serverless` directory\
	```npm install```

3. After it installs add the IAM account the `.aws` file which is usually located in `$HOME/.aws` \

4. The following changes will be required to all lambda functions:\
	&nbsp; a. Change the CORS Allow-Origin header to the URL of the target website (e.g. the cloudfront URL)\
	** this can be done after first deploy **
	
5. The following changes will be required in the `serverless.yml` file
&nbsp; a. update the account number to the account you created under iamRoleStatetments resources
&nbsp; b. Change the bucketName under custom to a unique name. 
	
6. Deploy the stack using `sls deploy`
	
7. If you've done it correctly the stack should deploy. \
&nbsp;a. If you have any issues deploying check permissions of the AWS account you created
	
### Firebase Service Account
serverless requires access to a firebase admin SDK, therefore requires a service account.

1. To create an account go to the firebase project settings -> service account tab
2. Generate a new private key.
3. Paste the contents of the key into the file `serviceAccountKey.JSON`
	

## Section 3 - IoT Core

IoT Core isn't configured via serverless therefore is required to be manually created, follow the below \
steps to set-up and configure it. The lambda function PlantMQTTLambda.js isn't used in serverless and must be manually \
created and a trigger must be made on the MQTT message. 

1. On IoT Core create a new thing, follow the set-up process and download the certificates \
&nbsp; a. The policy is below, change the account ID to the one you created previously \
	
```json	{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "iot:Connect",
      "Resource": "arn:aws:iot:ap-southeast-2:<Account ID>:client/PlantPot"
    },
    {
      "Effect": "Allow",
      "Action": "iot:Publish",
      "Resource": "arn:aws:iot:ap-southeast-2:<Account ID>:topic/SWE30012/water/data"
    },
    {
      "Effect": "Allow",
      "Action": "iot:Subscribe",
      "Resource": "arn:aws:iot:ap-southeast-2:<Account ID>:topicfilter/SWE30012/*"
    },
    {
      "Effect": "Allow",
      "Action": "iot:Receive",
      "Resource": "arn:aws:iot:ap-southeast-2:506641630323:topic/SWE30012/*"
    }
  ]
}
```


&nbsp; b. You will need to download the following certifcates: \
&nbsp;&nbsp;&nbsp;&nbsp; i. certificate.pem \
&nbsp;&nbsp;&nbsp;&nbsp; ii. private-pem.key \
&nbsp;&nbsp;&nbsp;&nbsp; iii. public-pem.key \
&nbsp;&nbsp;&nbsp;&nbsp; iv. AmazonRootCA1.pem

2. Following the policy the following MQTT topics are used:\
&nbsp;&nbsp;a. SWE30012/water/data - the publish topic used by the Plant Pot to send its data\
&nbsp;&nbsp;b. We use SWE30012/{PotId}/pump - the subscribe topic used by the Plant Pot and lambda to control the pump  \
&nbsp;&nbsp;&nbsp;**PotId is the mac-address of the ESP32** 

3. The following format is used in the Plant Pot MQTT publish message:

```
{
"PotId" : "mac-adress",
"Temperature" : "?",
"WaterLevel" :  "?",
"Moisture" :  "?",
"Humidity" : "?"
}

```

4. Create a trigger that uses a lambda function for the topic `SWE30012/water/data` \
&nbsp;&nbsp;a. rule statement: `SELECT * FROM 'SWE30012/water/data'`

5. The lambda function uses node and copy the code from PlantMQTTData.js in the `serverless/src` directory\
&nbsp;&nbsp; a. Assign it the IAM role used for serverless
&nbsp;&nbsp; b. deploy the changes

## Section 4 - End Point Request Format

| End point Path  | Format     |
| :------------- | :----------: | 
|  / |   ``` { "token" : "jwt-token, "PotId" : "mac-address", "PlantName" : "?", "PlantType" : "?" }``` |
| /fetch/all   | ``` {"token" : "jwt-token}"```|
| /fetch/single |``` {"token" : "jwt-token, "PotId" : "mac-address"}"```|
| /register | ``` {"token" : "jwt-token, "PhoneNumber" : "?"}"```|
| /sms |``` {"token" : "jwt-token}"```|
| /sms/verify |``` {"token" : "jwt-token", "OTP" : "?"}```|
| /update |``` {"token" : "jwt-token", "PotId" : "mac-address", "PlantStatus" : "?"}```| 

## Section 5 - Firebase Authentication

To configure the authentication for the website
1. Sign into Firebase and navigate to the [Project Overview page](https://console.firebase.google.com/u/0/)
2. Click 'Add Project'
3. Assign any project name
4. Google Analytics are not required for the project
5. When the project has been created, click the Authentication tab in the left side panel
6. Click 'Set up sign-in method'
7. Enable Google sign-in (this is the only sign-in method used by the site currently) and configure a support email
8. Click the cog icon in the left side panel and then Project Settings
9. Under 'Your Apps' click on the </> icon to make a web app
10. Assign an arbitrary name to the app, and you do not need to set up Firebase Hosting
11. Copy the const firebaseConfig object
12. Replace the firebaseConfig object in each HTML file with the copied object
13. Authentication is now configured into the website
 