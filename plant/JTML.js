var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var _JTML_ref;
// Josh Text Markup Language
// simplified DOM manipulation
// since (almost) all methods returns 'this' you can chain multiple methods one after the other
// last edited 2/10/2021
class JTML {
    constructor(type) {
        _JTML_ref.set(this, void 0);
        this.ref = type;
    }
    get ref() {
        return __classPrivateFieldGet(this, _JTML_ref, "f");
    }
    set ref(type) {
        if (/[.#:]\w+/.test(type)) {
            __classPrivateFieldSet(this, _JTML_ref, document.querySelector(type), "f");
        }
        else if (type instanceof Element) {
            __classPrivateFieldSet(this, _JTML_ref, type, "f");
        }
        else {
            __classPrivateFieldSet(this, _JTML_ref, document.createElement(type), "f");
        }
    }
    getHtml() {
        return this.ref.innerHTML;
    }
    get value() {
        if (__classPrivateFieldGet(this, _JTML_ref, "f") instanceof HTMLInputElement) {
            return __classPrivateFieldGet(this, _JTML_ref, "f").value;
        }
    }
    // needs to be a function so that we can return 'this'
    setValue(newValue) {
        if (__classPrivateFieldGet(this, _JTML_ref, "f") instanceof HTMLInputElement) {
            __classPrivateFieldGet(this, _JTML_ref, "f").value = newValue;
        }
        return this;
    }
    // set the innherHTML of the element, reminder that 
    // this will remove all event listeners on this element and its children
    html(value) {
        this.ref.innerHTML = value;
        return this;
    }
    // assign any arbitrary property
    set(key, value) {
        __classPrivateFieldGet(this, _JTML_ref, "f").setAttribute(key, value);
        return this;
    }
    // assign CSS style to this particular element, provided in JSON notation
    // e.g. {
    //    "background-color": "red",
    //    "opacity": 0.5
    // }
    css(cssObj) {
        for (const [key, value] of Object.entries(cssObj)) {
            this.ref.style[key] = value;
        }
        return this;
    }
    // // can be passed either a single key:value pair or an array of arrays in the format [[key,value],[key,value]]
    // css(key: string,value: string=null) {
    // 	if (Array.isArray(key)) {
    // 		key.forEach((pair)=>{
    // 			this.#ref.style[pair[0]] = pair[1];
    // 		})
    // 	} else {
    // 		this.#ref.style[key] = value;
    // 	}
    // 	return this;
    // }
    // cssToggle(state1:string,state2:string) {
    // 	if (this.#ref.style[state1[0]] == state1[1]) {
    // 		this.#ref.style[state2[0]] = state2[1];
    // 	} else {
    // 		this.#ref.style[state1[0]] = state1[1];
    // 	}
    // 	return this;
    // }
    // // same as css, but applies to hover state
    // hover(key,value=null) {
    // }
    // add a class to the element
    // this could be achieved using set() but its just cleaner this way and it's a property you often want to assign
    class(className) {
        if (Array.isArray(className)) {
            className.forEach((singleClass) => {
                __classPrivateFieldGet(this, _JTML_ref, "f").classList.add(singleClass);
            });
        }
        else {
            __classPrivateFieldGet(this, _JTML_ref, "f").classList.add(className);
        }
        return this;
    }
    // remove class from the element
    removeClass(className) {
        __classPrivateFieldGet(this, _JTML_ref, "f").classList.remove(className);
        return this;
    }
    toggleClass(className) {
        __classPrivateFieldGet(this, _JTML_ref, "f").classList.toggle(className);
        return this;
    }
    // append a child element to the end of this element
    add(JTMLarray) {
        if (Array.isArray(JTMLarray)) {
            JTMLarray.forEach((singleJTML) => {
                __classPrivateFieldGet(this, _JTML_ref, "f").append(singleJTML.ref);
            });
        }
        else {
            __classPrivateFieldGet(this, _JTML_ref, "f").append(JTMLarray.ref);
        }
        return this;
    }
    // append this element as the last child to another
    appendTo(parentElem) {
        parentElem.add(this);
        return this;
    }
    // passes all of the input parameters to addEventListener
    // just saves you from writing varName.ref.addEventListener
    addEventListener(type, listener, options) {
        __classPrivateFieldGet(this, _JTML_ref, "f").addEventListener(type, listener, options);
        return this;
    }
    // a duplicate of addEventListener, you can probably have them replicating each other
    // more cleanly but cant be bothered right now
    on(type, listener, options) {
        __classPrivateFieldGet(this, _JTML_ref, "f").addEventListener(type, listener, options);
        return this;
    }
    // removes all event listeners on this element and its children
    removeEventListeners() {
        var new_element = __classPrivateFieldGet(this, _JTML_ref, "f").cloneNode(true);
        __classPrivateFieldGet(this, _JTML_ref, "f").parentNode.replaceChild(new_element, this.ref);
        __classPrivateFieldSet(this, _JTML_ref, new_element, "f");
        return this;
    }
    // remove this element from the DOM
    remove() {
        this.ref.remove();
        return false;
    }
    // maybe try implementing this in a different way, but this version
    // would end up selecting unintuitive elements because querySelector operates depth-first
    // it should probably use the Element.children property because that's a shallow selection
    // and then perform it iteratively
    // // perform a querySelector for a given string
    // public child(childIdentifier: string) {
    // 	return new JTML(this.ref.querySelector(childIdentifier));
    // }
    // perform a querySelectorAll for a given string
    // by default select all children
    children(childIdentifier = '*') {
        let children = [];
        let preChildren = this.ref.querySelectorAll(childIdentifier);
        preChildren.forEach((singleChild) => {
            children.push(new JTML(singleChild));
        });
        return children;
    }
    parent() {
        return new JTML(this.ref.parentNode);
    }
}
_JTML_ref = new WeakMap();
