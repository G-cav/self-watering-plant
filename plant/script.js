"use strict";
var home;
(function (home) {
    const w = window;
    let loggedIn = false;
    async function init(user) {
        let authToken = await w.auth.currentUser.getIdToken(true);
        let dataToSend = {
            "token": authToken
        };
        let plants = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/fetch/all', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(dataToSend)
        }).then((res) => res.json());
        console.log("this is all of the plants associated with this user:");
        console.log(plants);
        let plantRows = new JTML('#plantRows');
        console.log(plants.Items);
        if (plants.Items.length == 0) {
            let noPlants = new JTML('p')
                .html(`you don't have any plants registered`)
                .appendTo(new JTML('#plantRows'))
                .css({ 'text-align': 'center',
                'margin': '20px',
                'font-size': '1.3em',
                'color': '#19475c' });
        }
        plants.Items.forEach((singlePlant) => {
            console.log(singlePlant);
            let rowContainer = new JTML('div')
                .class('rowContainer')
                .appendTo(plantRows);
            let plantNameContainer = new JTML('div')
                .css({ 'padding': '20px' })
                .appendTo(rowContainer);
            let plantName = new JTML('a')
                .class('plantName')
                .html(singlePlant.PlantName)
                .set('href', `pot/?p=${singlePlant.PotId}`)
                .appendTo(plantNameContainer);
            let plantStatus = new JTML('button')
                .class('plantStatus')
                .html(singlePlant.PlantStatus)
                .appendTo(rowContainer)
                .on('click', () => {
                window.location.href = `status/?p=${singlePlant.PotId}`;
            });
            if (singlePlant.PlantStatus == 'Healthy') {
                plantStatus.class("healthyPlantButton");
            }
            else {
                plantStatus.class("unhealthyPlantButton");
            }
        });
        let addPlantButton = new JTML('#addPlant')
            .addEventListener('click', () => {
            window.location.href = ("addPlant");
        });
    }
    w.onAuthStateChanged(w.auth, (user) => {
        if (user) {
            if (!loggedIn) {
                loggedIn = true;
                init(user);
            }
        }
        else {
            w.signInWithPopup(w.auth, w.provider)
                .then(async (result) => {
                const user = result.user;
                console.log(user);
                let authToken = await w.auth.currentUser.getIdToken(true);
                let loginData = {
                    "token": authToken
                };
                let loginResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/register', {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify(loginData)
                }).then((res) => res.json());
                console.log("this is the response after first logging in and telling the server:");
                console.log(loginResult);
            }).catch((error) => {
                alert(error);
            });
        }
    });
})(home || (home = {}));
