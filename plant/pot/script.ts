namespace addPlant {
"use strict";

let w = window as any;
let loggedIn = false;

let dummyTemperatures = [{
		temp:20,
		date: new Date('2021-03-12')
	},{
		temp:31,
		date: new Date('2021-03-13')
	},{
		temp:15,
		date: new Date('2021-03-14')
	},{
		temp:25,
		date: new Date('2021-03-15')
	},{
		temp:25,
		date: new Date('2021-03-16')
	},{
		temp:30,
		date: new Date('2021-03-17')
	},{
		temp:19,
		date: new Date('2021-03-18')
	},{
		temp:26,
		date: new Date('2021-03-19')
	},{
		temp:29,
		date: new Date('2021-03-20')
	},{
		temp:15,
		date: new Date('2021-03-21')
	},{
		temp:13,
		date: new Date('2021-03-22')
	},{
		temp:10,
		date: new Date('2021-03-23')
	}];

async function viewPlant(user) {
	let currentPotID = new URL(window.location.href).searchParams.get('p');
	let currentPlant;
	let authToken = await w.auth.currentUser.getIdToken(true)
	let dataToSend = {
		"token": authToken,
		"PotId": currentPotID
	}
	let result = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/fetch/single', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify(dataToSend)
	}).then((res) => res.json());
	console.log(`this is all of the data associated with the pot '${currentPotID}', which is associated with this user:`)
	console.log(result);
	currentPlant = result.Items[0];

	let potName = new JTML('#potName')
		.html(currentPlant.PlantName);

	let statusUpdateLink = new JTML('#statusUpdateLink')
		.set('href',`/plant/status/?p=${currentPotID}`)

	let plantStatus = new JTML('#plantStatus')
		.css({'margin':'20px'})
		.html(currentPlant.PlantStatus)
		.on('click',()=>{
			window.location.href = `/plant/status/?p=${currentPotID}`;
		})

	if (currentPlant.PlantStatus == 'healthy') {
		plantStatus.class("healthyPlantButton")
	} else {
		plantStatus.class("unhealthyPlantButton")
	}

	let jraph = new Jraph(dummyTemperatures,'temp')
		.setYSpacing('%')
		.setXSpacing(200)
		.renderWithResizeListener('#graph');
}

w.onAuthStateChanged(w.auth, (user) => {
	if (user) {
		if (!loggedIn) {
			loggedIn = true;
			viewPlant(user);
		}
	} else {
		w.signInWithPopup(w.auth, w.provider)
		.then(async (result) => {
			const user = result.user;
			console.log(user);
			let authToken = await w.auth.currentUser.getIdToken(true)
			let loginData = {
				"token": authToken
			}
			let loginResult = await fetch('https://mov47uz35b.execute-api.ap-southeast-2.amazonaws.com/register', {
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(loginData)
			}).then((res) => res.json());
			console.log("this is the response after first logging in and telling the server:")
			console.log(loginResult);
		}).catch((error) => {
			alert(error);
		});	
	}
});

}

