var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Jraph_data, _Jraph_xAxis, _Jraph_yAxis, _Jraph_pointShape, _Jraph_knownShapes, _Jraph_orientation, _Jraph_xVisualIncrement, _Jraph_xNumericalIncrement, _Jraph_yVisualIncrement, _Jraph_yNumericalIncrement, _Jraph_horizontalPadding, _Jraph_verticalPadding, _Jraph_container, _Circle_radius, _Circle_reference;
class Jraph {
    constructor(data, yAxis = 'value', xAxis = 'date') {
        _Jraph_data.set(this, void 0);
        _Jraph_xAxis.set(this, void 0);
        _Jraph_yAxis.set(this, void 0);
        _Jraph_pointShape.set(this, void 0);
        _Jraph_knownShapes.set(this, new Map());
        // true = upright, false = vertical
        _Jraph_orientation.set(this, true);
        // make these more generalised when working with other values
        _Jraph_xVisualIncrement.set(this, void 0);
        _Jraph_xNumericalIncrement.set(this, 604800000); // this is one week in miliseconds
        _Jraph_yVisualIncrement.set(this, void 0);
        _Jraph_yNumericalIncrement.set(this, 1); // this is 1kg ?????
        _Jraph_horizontalPadding.set(this, 1);
        _Jraph_verticalPadding.set(this, 20);
        _Jraph_container.set(this, void 0);
        __classPrivateFieldSet(this, _Jraph_data, data, "f");
        __classPrivateFieldSet(this, _Jraph_xAxis, xAxis, "f");
        __classPrivateFieldSet(this, _Jraph_yAxis, yAxis, "f");
        this.registerShape(Circle);
    }
    get yMin() {
        return Math.min(...this.allValues(__classPrivateFieldGet(this, _Jraph_yAxis, "f")));
    }
    get yMax() {
        return Math.max(...this.allValues(__classPrivateFieldGet(this, _Jraph_yAxis, "f")));
    }
    get xMin() {
        return Math.min(...this.allValues(__classPrivateFieldGet(this, _Jraph_xAxis, "f")));
    }
    get xMax() {
        return Math.max(...this.allValues(__classPrivateFieldGet(this, _Jraph_xAxis, "f")));
    }
    get xRange() {
        let range = this.xMax - this.xMin;
        // we often divide by the range, so it can't be 0
        // the value returned is currently chosen randomly because typically it's 0/range
        // so it doesn't matter what the value is
        if (range == 0) {
            return 1;
        }
        return range;
    }
    get yRange() {
        let range = this.yMax - this.yMin;
        // we often divide by the range, so it can't be 0
        // the value returned is currently chosen randomly because typically it's 0/range
        // so it doesn't matter what the value is
        if (range == 0) {
            return 1;
        }
        return range;
    }
    get xSpacing() {
        if (__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%') {
            return 100;
        }
        return Number(__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f"));
    }
    get ySpacing() {
        if (__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%') {
            return 100;
        }
        return Number(__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f"));
    }
    allValues(property) {
        return __classPrivateFieldGet(this, _Jraph_data, "f").map((singleData) => {
            return singleData[property];
        });
    }
    registerShape(shapeType) {
        __classPrivateFieldGet(this, _Jraph_knownShapes, "f").set(shapeType.name, shapeType);
        // set a default shape, based on the first shape introduced
        if (__classPrivateFieldGet(this, _Jraph_knownShapes, "f").size == 1) {
            __classPrivateFieldSet(this, _Jraph_pointShape, shapeType.name, "f");
        }
    }
    setOrientation(newOrientation) {
        __classPrivateFieldSet(this, _Jraph_orientation, newOrientation, "f");
        return this;
    }
    setYSpacing(providedSpacing) {
        if (this.performChecks(providedSpacing)) {
            __classPrivateFieldSet(this, _Jraph_yVisualIncrement, providedSpacing, "f");
            return this;
        }
        return null;
    }
    setXSpacing(providedSpacing) {
        if (this.performChecks(providedSpacing)) {
            __classPrivateFieldSet(this, _Jraph_xVisualIncrement, providedSpacing, "f");
            return this;
        }
        return null;
    }
    performChecks(providedSpacing) {
        // only allow for numbers and % signs
        if (providedSpacing == '%') {
            return true;
        }
        else if (!isNaN(Number(providedSpacing))) {
            return true;
        }
        return null;
    }
    containerHeight() {
        return __classPrivateFieldGet(this, _Jraph_container, "f").parent().ref.clientHeight;
    }
    // you probably don't need three different methods to get the width, so figure out which one is redundant
    containerWidth() {
        return __classPrivateFieldGet(this, _Jraph_container, "f").parent().ref.clientWidth;
    }
    // relativeHeight is either is the size of the parent container in pixels, or it's 100%
    getRelativeHeight() {
        if (__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%') {
            return 100;
        }
        return this.containerHeight();
    }
    // you probably don't need three different methods to get the width, so figure out which one is redundant
    // relativeWidth is either is the size of the parent container in pixels, or it's 100%
    getRelativeWidth() {
        if (__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%') {
            return 100;
        }
        return this.containerWidth();
    }
    // you probably don't need three different methods to get the width, so figure out which one is redundant
    getIdealWidth() {
        let contentSize = 2 * this.xSpacing * __classPrivateFieldGet(this, _Jraph_horizontalPadding, "f") + (this.xRange / __classPrivateFieldGet(this, _Jraph_xNumericalIncrement, "f")) * this.xSpacing;
        return Math.max(contentSize, this.containerWidth());
    }
    // this could maybe be upgraded out allow an array of outputs
    getCoordForPoint(axis, point, output) {
        if (axis == 'x') {
            let acceptableWidth = this.getRelativeWidth() - 2 * __classPrivateFieldGet(this, _Jraph_horizontalPadding, "f");
            let horizontalPaddingRatio = acceptableWidth / this.getRelativeWidth();
            let xOffsetFromMin = point - this.xMin;
            // either normalised relative to the numericalIncrement, or the max value
            let xNormalisedOffset;
            if (__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%') {
                xNormalisedOffset = xOffsetFromMin / this.xRange;
            }
            else {
                xNormalisedOffset = xOffsetFromMin / __classPrivateFieldGet(this, _Jraph_xNumericalIncrement, "f");
            }
            let xScaledOffset = xNormalisedOffset * this.xSpacing;
            let xWithPadding = xScaledOffset + this.xSpacing * __classPrivateFieldGet(this, _Jraph_horizontalPadding, "f");
            if (output == "stringPixelsOrPercentage") {
                return `${xWithPadding}${__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%' ? '%' : ''}`;
            }
            else if (output == "stringPixelsOnly") {
                return __classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%' ? (xWithPadding / 100) * this.containerWidth() : xWithPadding;
            }
        }
        else {
            let acceptableHeight = this.getRelativeHeight() - 2 * __classPrivateFieldGet(this, _Jraph_verticalPadding, "f");
            let verticalPaddingRatio = acceptableHeight / this.getRelativeHeight();
            let yOffsetFromMin = point - this.yMin;
            // normalised relative to the numericalIncrement, not normalised relative to the max value
            let yNormalisedOffset;
            if (__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%') {
                yNormalisedOffset = yOffsetFromMin / this.yRange;
            }
            else {
                yNormalisedOffset = yOffsetFromMin / __classPrivateFieldGet(this, _Jraph_yNumericalIncrement, "f");
            }
            let yScaledOffset = yNormalisedOffset * this.ySpacing;
            let yWithPadding = yScaledOffset * verticalPaddingRatio + __classPrivateFieldGet(this, _Jraph_verticalPadding, "f");
            // flip it because you're used to defining coordinates relative to the lower left corner, rather than upper left
            let yFlippedUpright = this.getRelativeHeight() - yWithPadding;
            if (output == "stringPixelsOrPercentage") {
                return `${yFlippedUpright}${__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%' ? '%' : ''}`;
            }
            else if (output == "stringPixelsOnly") {
                return __classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%' ? (yFlippedUpright / 100) * this.containerHeight() : yFlippedUpright;
                // } else if (output=="number") {
                // 	return yFlippedUpright;
            }
        }
    }
    renderWithResizeListener(selector) {
        __classPrivateFieldSet(this, _Jraph_container, new JTML(selector), "f");
        new ResizeObserver(() => {
            this.renderOnce();
        }).observe(__classPrivateFieldGet(this, _Jraph_container, "f").parent().ref);
    }
    // to improve this, have JTML operate like a regular CSS selector and only create new elements if a particular method is called
    renderOnce(container = __classPrivateFieldGet(this, _Jraph_container, "f")) {
        // clear the container before adding elements
        container.html('');
        if (__classPrivateFieldGet(this, _Jraph_yVisualIncrement, "f") == '%') {
            container.set('height', '100%');
            // } else {
            // 	container.set('height',`${this.yRange}%`);
        }
        if (__classPrivateFieldGet(this, _Jraph_xVisualIncrement, "f") == '%') {
            container.set('width', '100%');
        }
        else {
            container.set('width', `${this.getIdealWidth()}px`);
        }
        let pathString = `M `;
        __classPrivateFieldGet(this, _Jraph_data, "f").forEach((dataPoint, i) => {
            dataPoint.xValueWithSymbol = this.getCoordForPoint('x', dataPoint[__classPrivateFieldGet(this, _Jraph_xAxis, "f")], "stringPixelsOrPercentage");
            dataPoint.yValueWithSymbol = this.getCoordForPoint('y', dataPoint[__classPrivateFieldGet(this, _Jraph_yAxis, "f")], "stringPixelsOrPercentage");
            // in the percentage case, we need to convert from a value 0-100 to a value 0-maximumPixelsAlongAxis
            pathString += `${this.getCoordForPoint('x', dataPoint[__classPrivateFieldGet(this, _Jraph_xAxis, "f")], "stringPixelsOnly")} ` +
                `${this.getCoordForPoint('y', dataPoint[__classPrivateFieldGet(this, _Jraph_yAxis, "f")], "stringPixelsOnly")} L `;
        });
        pathString = pathString.substring(0, pathString.length - 2);
        this.renderAxes(container);
        this.renderStroke(container, pathString);
        this.renderShape(container);
        return this;
    }
    renderLine(container, x1, y1, x2, y2, colour) {
        let newLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
        // topLine.id = name + "line";  
        container.ref.append(newLine);
        newLine.setAttribute("x1", x1);
        newLine.setAttribute("x2", x2);
        newLine.setAttribute("y1", y1);
        newLine.setAttribute("y2", y2);
        newLine.setAttribute("style", `stroke:${colour};stroke-width:1`);
    }
    renderAxes(container) {
        console.log("iiiiiiiiiiiiiii");
        let xCoord = 0;
        // put markers on the same day every week, starting at whatever the first data point is
        for (let i = 0; xCoord < this.getIdealWidth() || i < Math.ceil(this.xRange / __classPrivateFieldGet(this, _Jraph_xNumericalIncrement, "f")); i++) {
            // this should be upgraded to take advantage of the getCoordForPoint method
            xCoord = i * this.xSpacing + this.xSpacing * __classPrivateFieldGet(this, _Jraph_horizontalPadding, "f");
            console.log(xCoord);
            // axisColour should be defined in Jraph, it's not as good to do it in CSS separately because then you can't have multiple graphs
            this.renderLine(container, xCoord, 0, xCoord, this.containerHeight(), 'var(--axisColour)');
        }
        let increment = 1;
        // keep in mind that yRange would be 0 sometimes, however because we're often dividing by it, you assign a random other value which currently is 1
        if (this.yRange < 1) {
            increment = this.yRange;
        }
        // probably a cleaner way to do this, but you're just starting at the lowest x-point and moving up until it hits the top,
        // and then likewise starting at the lowest x-point and moving down until you hit the bottom, to get the padding
        let yCoord = 0;
        for (let i = 0; yCoord >= 0 && yCoord <= this.containerHeight(); i += increment) {
            yCoord = this.getCoordForPoint('y', this.yMin + i, "stringPixelsOnly");
            this.renderLine(container, 0, yCoord, this.getIdealWidth(), yCoord, 'var(--axisColour)');
        }
        yCoord = 0;
        for (let i = -1; yCoord >= 0 && yCoord <= this.containerHeight(); i -= increment) {
            yCoord = this.getCoordForPoint('y', this.yMin + i, "stringPixelsOnly");
            this.renderLine(container, 0, yCoord, this.getIdealWidth(), yCoord, 'var(--axisColour)');
        }
        console.log("jjjjjjjjjjj");
    }
    renderStroke(container, strokeString) {
        let pathToAdd = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        container.ref.append(pathToAdd);
        pathToAdd.setAttribute("d", strokeString);
        pathToAdd.setAttribute("fill", "none");
        pathToAdd.setAttribute("stroke", "var(--strokeColour)");
        // strokeWidth should be defined in the Jraph, currently it's just being done in the adjacent CSS which isn't as good
        pathToAdd.setAttribute("stroke-width", "var(--strokeWidth)");
    }
    renderShape(container) {
        __classPrivateFieldGet(this, _Jraph_data, "f").forEach((dataPoint) => {
            let shapeType = __classPrivateFieldGet(this, _Jraph_knownShapes, "f").get(__classPrivateFieldGet(this, _Jraph_pointShape, "f"));
            let newShape = new shapeType(dataPoint.xValueWithSymbol, dataPoint.yValueWithSymbol)
                .renderTo(container)
                // this shouldn't be in here, this is unique to the workout project
                .addData(`<h3 style="margin-bottom:10px; color: #aac7d2">${dataPoint.dateString}</h3>
				<p><span class="bold" style="margin-left:0px; margin-bottom:10px">${dataPoint.reps}</span> reps at <span class="bold">${dataPoint.weight}</span> kg</p><span class="verticallyCentered"><p style="padding-right: 11px;">and found it</p> <span>${dataPoint.feelingImage}</span></span>`);
        });
    }
}
_Jraph_data = new WeakMap(), _Jraph_xAxis = new WeakMap(), _Jraph_yAxis = new WeakMap(), _Jraph_pointShape = new WeakMap(), _Jraph_knownShapes = new WeakMap(), _Jraph_orientation = new WeakMap(), _Jraph_xVisualIncrement = new WeakMap(), _Jraph_xNumericalIncrement = new WeakMap(), _Jraph_yVisualIncrement = new WeakMap(), _Jraph_yNumericalIncrement = new WeakMap(), _Jraph_horizontalPadding = new WeakMap(), _Jraph_verticalPadding = new WeakMap(), _Jraph_container = new WeakMap();
class JShape {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class Circle extends JShape {
    constructor(x, y, data, radius = "10") {
        super(x, y);
        _Circle_radius.set(this, void 0);
        _Circle_reference.set(this, void 0);
        __classPrivateFieldSet(this, _Circle_radius, radius, "f");
        __classPrivateFieldSet(this, _Circle_reference, document.createElementNS("http://www.w3.org/2000/svg", 'circle'), "f");
    }
    renderTo(container) {
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute('cx', this.x);
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute('cy', this.y);
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute('r', __classPrivateFieldGet(this, _Circle_radius, "f"));
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute("stroke", "var(--strokeColour)");
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute("stroke-width", "var(--strokeWidth)");
        container.ref.append(__classPrivateFieldGet(this, _Circle_reference, "f"));
        console.log(this);
        return this;
    }
    addData(data) {
        __classPrivateFieldGet(this, _Circle_reference, "f").setAttribute('data-a', data);
        return this;
    }
}
_Circle_radius = new WeakMap(), _Circle_reference = new WeakMap();
