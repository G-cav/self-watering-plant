type ClassType = new (...args: any[]) => any;

class Jraph {
	#data: Array<any>;
	#xAxis: string;
	#yAxis: string;

	#pointShape: string;
	#knownShapes = new Map<string,ClassType>();

	// true = upright, false = vertical
	#orientation = true;

	// make these more generalised when working with other values
	#xVisualIncrement: number | string;
	#xNumericalIncrement = 604800000; // this is one week in miliseconds
	#yVisualIncrement: number | string;
	#yNumericalIncrement = 1;  // this is 1kg ?????

	#horizontalPadding = 1;
	#verticalPadding = 20;

	#container: JTML;
	

	constructor(data: Array<any>, yAxis = 'value', xAxis = 'date') {
		this.#data  = data;
		this.#xAxis = xAxis;
		this.#yAxis = yAxis;

		this.registerShape(Circle);
	}

	get yMin() {
		return Math.min(...(<number[]>this.allValues(this.#yAxis))); 
	}

	get yMax() {
		return Math.max(...(<number[]>this.allValues(this.#yAxis))); 
	}

	get xMin() {
		return Math.min(...(<number[]>this.allValues(this.#xAxis))); 
	}

	get xMax() {
		return Math.max(...(<number[]>this.allValues(this.#xAxis))); 
	}

	get xRange() {
		let range = this.xMax - this.xMin;
		// we often divide by the range, so it can't be 0
		// the value returned is currently chosen randomly because typically it's 0/range
		// so it doesn't matter what the value is
		if (range == 0) {
			return 1;
		}
		return range;
	}

	get yRange() {
		let range = this.yMax - this.yMin;
		// we often divide by the range, so it can't be 0
		// the value returned is currently chosen randomly because typically it's 0/range
		// so it doesn't matter what the value is
		if (range == 0) {
			return 1;
		}
		return range;
	}

	get xSpacing() {
		if (this.#xVisualIncrement == '%') {
			return 100;
		} 
		return Number(this.#xVisualIncrement);
	}

	get ySpacing() {
		if (this.#yVisualIncrement == '%') {
			return 100;
		} 
		return Number(this.#yVisualIncrement);
	}


	private allValues(property: string) {
		return this.#data.map((singleData) => {
			return singleData[property];
		});
	}

	public registerShape(shapeType: ClassType) {
		this.#knownShapes.set(shapeType.name, shapeType);
		// set a default shape, based on the first shape introduced
		if (this.#knownShapes.size == 1) {
			this.#pointShape = shapeType.name;
		}
	}

	public setOrientation(newOrientation: boolean) {
		this.#orientation = newOrientation;
		return this;
	}

	public setYSpacing(providedSpacing: number | string) {
		if (this.performChecks(providedSpacing)) {
			this.#yVisualIncrement = providedSpacing;
			return this;
		}
		return null;
	}

	public setXSpacing(providedSpacing: number | string) {
		if (this.performChecks(providedSpacing)) {
			this.#xVisualIncrement = providedSpacing;
			return this;
		}
		return null;
	}

	private performChecks(providedSpacing: number | string) {
		// only allow for numbers and % signs
		if (providedSpacing == '%') {
			return true;
		} else if (!isNaN(Number(providedSpacing))) {
			return true;
		}
		return null;
	}

	private containerHeight() {
		return this.#container.parent().ref.clientHeight;
	}

	// you probably don't need three different methods to get the width, so figure out which one is redundant
	private containerWidth() {
		return this.#container.parent().ref.clientWidth;
	}

	// relativeHeight is either is the size of the parent container in pixels, or it's 100%
	private getRelativeHeight() {
		if (this.#yVisualIncrement=='%') {
			return 100;
		}
		return this.containerHeight();
	}

	// you probably don't need three different methods to get the width, so figure out which one is redundant
	// relativeWidth is either is the size of the parent container in pixels, or it's 100%
	private getRelativeWidth() {
		if (this.#xVisualIncrement=='%') {
			return 100;
		}
		return this.containerWidth();
	}

	// you probably don't need three different methods to get the width, so figure out which one is redundant
	private getIdealWidth() {
		let contentSize = 2*this.xSpacing*this.#horizontalPadding + (this.xRange/this.#xNumericalIncrement)*this.xSpacing;
		return Math.max(contentSize, this.containerWidth());
	}

	// this could maybe be upgraded out allow an array of outputs
	private getCoordForPoint(axis: string, point: number, output?: string) {
		if (axis == 'x') {
			let acceptableWidth = this.getRelativeWidth() - 2*this.#horizontalPadding;
			let horizontalPaddingRatio = acceptableWidth/this.getRelativeWidth();
			let xOffsetFromMin = point-this.xMin;
			// either normalised relative to the numericalIncrement, or the max value
			let xNormalisedOffset;
			if (this.#xVisualIncrement=='%') {
				xNormalisedOffset = xOffsetFromMin/this.xRange;
			} else {
				xNormalisedOffset = xOffsetFromMin/this.#xNumericalIncrement;
			}
			let xScaledOffset = xNormalisedOffset*this.xSpacing;
			let xWithPadding = xScaledOffset + this.xSpacing*this.#horizontalPadding;

			if (output=="stringPixelsOrPercentage") {
				return `${xWithPadding}${this.#xVisualIncrement=='%'?'%':''}`;
			} else if (output=="stringPixelsOnly") {
				return this.#xVisualIncrement=='%'?(xWithPadding/100)*this.containerWidth():xWithPadding;
			}

		} else {
			let acceptableHeight = this.getRelativeHeight() - 2*this.#verticalPadding;
			let verticalPaddingRatio = acceptableHeight/this.getRelativeHeight();
			let yOffsetFromMin = point-this.yMin;
			// normalised relative to the numericalIncrement, not normalised relative to the max value
			let yNormalisedOffset;
			if (this.#yVisualIncrement=='%') {
				yNormalisedOffset = yOffsetFromMin/this.yRange;
			} else {
				yNormalisedOffset = yOffsetFromMin/this.#yNumericalIncrement;
			}
			let yScaledOffset = yNormalisedOffset*this.ySpacing;
			let yWithPadding = yScaledOffset*verticalPaddingRatio + this.#verticalPadding;

			// flip it because you're used to defining coordinates relative to the lower left corner, rather than upper left
			let yFlippedUpright = this.getRelativeHeight() - yWithPadding;

			if (output=="stringPixelsOrPercentage") {
				return `${yFlippedUpright}${this.#yVisualIncrement=='%'?'%':''}`;
			} else if (output=="stringPixelsOnly") {
				return this.#yVisualIncrement=='%'?(yFlippedUpright/100)*this.containerHeight():yFlippedUpright;
			// } else if (output=="number") {
			// 	return yFlippedUpright;
			}
		}
		
	}

	public renderWithResizeListener(selector: string) {
		this.#container = new JTML(selector);
		new ResizeObserver(()=>{
			this.renderOnce();
		}).observe(this.#container.parent().ref);
	}

	// to improve this, have JTML operate like a regular CSS selector and only create new elements if a particular method is called
	public renderOnce(container: JTML = this.#container) {
		// clear the container before adding elements
		container.html('');

		if (this.#yVisualIncrement=='%') {
			container.set('height','100%');
		// } else {
		// 	container.set('height',`${this.yRange}%`);
		}
		
		if (this.#xVisualIncrement=='%') {
			container.set('width','100%');
		} else {
			container.set('width',`${this.getIdealWidth()}px`);
		}
		

		let pathString = `M `;
		this.#data.forEach((dataPoint, i)=>{
			dataPoint.xValueWithSymbol = this.getCoordForPoint('x',dataPoint[this.#xAxis],"stringPixelsOrPercentage");
			dataPoint.yValueWithSymbol = this.getCoordForPoint('y',dataPoint[this.#yAxis],"stringPixelsOrPercentage");	

			// in the percentage case, we need to convert from a value 0-100 to a value 0-maximumPixelsAlongAxis
			pathString += `${this.getCoordForPoint('x',dataPoint[this.#xAxis],"stringPixelsOnly")} ` +
			              `${this.getCoordForPoint('y',dataPoint[this.#yAxis],"stringPixelsOnly")} L `;
		})
		pathString = pathString.substring(0, pathString.length - 2);

		this.renderAxes(container);
		this.renderStroke(container,pathString);
		this.renderShape(container);

		return this;
	}

	private renderLine(container,x1,y1,x2,y2,colour) {
		let newLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
		// topLine.id = name + "line";  
		container.ref.append(newLine);
		newLine.setAttribute("x1",x1);
		newLine.setAttribute("x2",x2);
		newLine.setAttribute("y1",y1);
		newLine.setAttribute("y2",y2);
		newLine.setAttribute("style",`stroke:${colour};stroke-width:1`);
	}

	private renderAxes(container) {
		console.log("iiiiiiiiiiiiiii")
		let xCoord = 0;
		// put markers on the same day every week, starting at whatever the first data point is
		for (let i = 0; xCoord < this.getIdealWidth() || i < Math.ceil(this.xRange/this.#xNumericalIncrement); i++) {
			// this should be upgraded to take advantage of the getCoordForPoint method
			xCoord = i*this.xSpacing+this.xSpacing*this.#horizontalPadding;
			console.log(xCoord);
			// axisColour should be defined in Jraph, it's not as good to do it in CSS separately because then you can't have multiple graphs
			this.renderLine(container,xCoord,0,xCoord,this.containerHeight(),'var(--axisColour)');
		}

		let increment = 1;
		// keep in mind that yRange would be 0 sometimes, however because we're often dividing by it, you assign a random other value which currently is 1
		if (this.yRange < 1) {
			increment = this.yRange;
		}
		// probably a cleaner way to do this, but you're just starting at the lowest x-point and moving up until it hits the top,
		// and then likewise starting at the lowest x-point and moving down until you hit the bottom, to get the padding
		let yCoord = 0;
		for (let i = 0; yCoord >= 0 && yCoord <= this.containerHeight(); i+=increment) {
			yCoord = <number>this.getCoordForPoint('y',this.yMin+i,"stringPixelsOnly");
			this.renderLine(container,0,yCoord,this.getIdealWidth(),yCoord,'var(--axisColour)');
		}
		yCoord = 0;
		for (let i = -1; yCoord >= 0 && yCoord <= this.containerHeight(); i-=increment) {
			yCoord = <number>this.getCoordForPoint('y',this.yMin+i,"stringPixelsOnly");
			this.renderLine(container,0,yCoord,this.getIdealWidth(),yCoord,'var(--axisColour)');
		}
		console.log("jjjjjjjjjjj")
	}

	private renderStroke(container: JTML, strokeString: string) {
		let pathToAdd = document.createElementNS("http://www.w3.org/2000/svg", 'path');
		container.ref.append(pathToAdd);
		pathToAdd.setAttribute("d", strokeString);
		pathToAdd.setAttribute("fill", "none");
		pathToAdd.setAttribute("stroke", "var(--strokeColour)");
		// strokeWidth should be defined in the Jraph, currently it's just being done in the adjacent CSS which isn't as good
		pathToAdd.setAttribute("stroke-width", "var(--strokeWidth)");
	}

	private renderShape(container: JTML) {
		this.#data.forEach((dataPoint)=>{
			let shapeType = this.#knownShapes.get(this.#pointShape);
			let newShape = new shapeType(dataPoint.xValueWithSymbol,dataPoint.yValueWithSymbol)
				.renderTo(container)
				// this shouldn't be in here, this is unique to the workout project
				.addData(`<h3 style="margin-bottom:10px; color: #aac7d2">${dataPoint.dateString}</h3>
				<p><span class="bold" style="margin-left:0px; margin-bottom:10px">${dataPoint.reps}</span> reps at <span class="bold">${dataPoint.weight}</span> kg</p><span class="verticallyCentered"><p style="padding-right: 11px;">and found it</p> <span>${dataPoint.feelingImage}</span></span>`);
		})		
	}
}



abstract class JShape {
	protected x: string;
	protected y: string;

	constructor(x: string, y: string) {
		this.x = x;
		this.y = y;
	}

	abstract renderTo(container: JTML);
}

class Circle extends JShape {
	#radius: string;
	#reference;

	constructor(x: string, y: string, data: string, radius = "10") {
		super(x,y);
		this.#radius = radius;
		this.#reference = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
	}

	public renderTo(container: JTML) {
		this.#reference.setAttribute('cx',this.x);
		this.#reference.setAttribute('cy',this.y);
		this.#reference.setAttribute('r',this.#radius);
		this.#reference.setAttribute("stroke", "var(--strokeColour)");
		this.#reference.setAttribute("stroke-width", "var(--strokeWidth)");
		container.ref.append(this.#reference)
		console.log(this);
		return this;
	}

	public addData(data: string) {
		this.#reference.setAttribute('data-a',data);
		return this;
	}
}

